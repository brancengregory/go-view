package view

import (
	"html/template"
	"net/http"
	"os"
	"path/filepath"
)

var (
	rootTemplate string
	viewInfo     View
)

//Template root
type Template struct {
	Root string
}

//View attributes
type View struct {
	BaseURI   string
	Extension string
	Folder    string
	Name      string
	Vars      map[string]interface{}
	request   *http.Request
}

// Configure sets the view information
func Configure(v View) {
	viewInfo = v
}

//New returns a new View
func New(req *http.Request) *View {
	v := &View{}
	v.Vars = make(map[string]interface{})
	v.Vars["AuthLevel"] = "anon"

	v.BaseURI = viewInfo.BaseURI
	v.Extension = viewInfo.Extension
	v.Folder = viewInfo.Folder
	v.Name = viewInfo.Name

	// Make sure BaseURI is available in the templates
	v.Vars["BaseURI"] = v.BaseURI

	// This is required for the view to access the request
	v.request = req

	// Get session
	//sess := session.Instance(v.request)

	// Set the AuthLevel to auth if the user is logged in
	/* 	if sess.Values["id"] != nil {
	   		v.Vars["AuthLevel"] = "auth"
	   	}
	*/
	return v
}

//Render renders a template to the writer
func (v *View) Render(w http.ResponseWriter) {
	var templateList []string
	templateList = append(templateList, rootTemplate)
	templateList = append(templateList, v.Name)

	// Loop through each template and test the full path
	for i, name := range templateList {
		// Get the absolute path of the root template
		path, err := filepath.Abs(v.Folder + string(os.PathSeparator) + name + "." + v.Extension)
		if err != nil {
			http.Error(w, "Template Path Error: "+err.Error(), http.StatusInternalServerError)
			return
		}
		templateList[i] = path
	}

	// Determine if there is an error in the template syntax
	templates, err := template.New(v.Name).ParseFiles(templateList...)
	if err != nil {
		http.Error(w, "Template Parse Error: "+err.Error(), http.StatusInternalServerError)
		return
	}

	//sess := session.Instance(v.request)

	// Display the content to the screen
	err = templates.ExecuteTemplate(w, rootTemplate+"."+v.Extension, v.Vars)
	if err != nil {
		http.Error(w, "Template File Error: "+err.Error(), http.StatusInternalServerError)
	}
}

// LoadTemplates will set the root template
func LoadTemplates(rootTemp string) {
	rootTemplate = rootTemp
}
